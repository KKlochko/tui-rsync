from behave import given, when, then


@given('the CLI arguments are "{arguments}"')
def given_cli_arguments(context, arguments):
    context.arguments = arguments.split()


@when('I run the CLI')
def when_run_cli(context):
    context.cli_result = context.cli_runner.invoke(context.cli_app, context.arguments)


@then('the CLI executed with "{result}"')
def then_cli_executed_successfully(context, result):
    match result:
        case "success":
            assert context.cli_result.exit_code == 0
        case "error":
            assert context.cli_result.exit_code != 0


@then('the CLI output contains "{string}"')
def then_cli_output_contains(context, string):
    print(f"Got: {context.cli_result.stdout}")
    assert string in context.cli_result.stdout


@then('the CLI output doesn\'t contains "{string}"')
def then_cli_output_contains(context, string):
    print(f"Got: {context.cli_result.stdout}")
    assert not (string in context.cli_result.stdout)


@then('the CLI contains the error: "{string}"')
def then_cli_output_contains_error(context, string):
    print(f"Got: {context.cli_result.stdout}")
    assert string in context.cli_result.stdout

