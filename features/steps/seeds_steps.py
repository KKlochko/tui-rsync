from behave import given


@given('I have a backup plan with id="{existing_backup_plan_id}"')
def given_existing_backup_plan_id_seed(context, existing_backup_plan_id):
    context.data_seeds.create_backup_plan(existing_backup_plan_id)

