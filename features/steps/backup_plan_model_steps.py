from behave import given, when, then
import json

from features.support.backup_plan_helpers import compare_destinations, update_backup_plan, compare_backup_plan_fields
from features.support.helpers import json_to_dict
from tui_rsync.core.components.backup_plan.application.services import RemoveAllBackupPlansService
from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from tui_rsync.core.components.backup_plan.domain import BackupPlan, Source, Destination
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.core.shared_kernel.ports.Exceptions import CommandException
from tui_rsync.core.shared_kernel.ports.Exceptions.query_exception import QueryException


@given('the label "{label}"')
def given_source_label(context, label):
    context.label = label


@given('the path "{source_path}"')
def given_source_path(context, source_path):
    context.source_path = source_path


@given('the destinations {destinations_json}')
def given_source_destinations(context, destinations_json):
    context.destinations = json.loads(destinations_json)


@when('I create the backup plan')
def add_backup_plan(context):
    try:
        context.backup_plan = BackupPlan(
            label=context.label,
            source=Source(context.source_path),
            destinations=list(map(lambda path: Destination(path), context.destinations)),
        )

        context.backup_plan_service.add(context.backup_plan)

        context.backup_plan = context.backup_plan_service.get_by_id(context.backup_plan.id)
    except Exception as e:
        context.exception_raised = True
    else:
        context.exception_raised = False


@when('I update the backup plan with id="{backup_plan_id}" and data="{updated_data_json}"')
def when_update_backup_plan_with_id(context, backup_plan_id, updated_data_json):
    try:
        context.backup_plan_service = context.injector.get(BackupPlanService)
        context.backup_plan = context.backup_plan_service.get_by_id(UUID(backup_plan_id))

        update_backup_plan(context.backup_plan, json_to_dict(updated_data_json))

        context.backup_plan_service.update(context.backup_plan)
    except QueryException as e:
        context.exception_raised = True
    else:
        context.exception_raised = False


@when('I remove the backup plan with id="{backup_plan_id}"')
def when_remove_backup_plan_with_id(context, backup_plan_id):
    try:
        context.backup_plan_service = context.injector.get(BackupPlanService)
        context.backup_plan_service.delete(UUID(backup_plan_id))
    except CommandException as e:
        context.exception_raised = True
    else:
        context.exception_raised = False


@when('I remove all backup plans')
def when_remove_all_backup_plans(context):
    try:
        context.backup_plan_service = context.injector.get(RemoveAllBackupPlansService)
        context.result_value = context.backup_plan_service.remove_all()
    except CommandException as e:
        context.exception_raised = True
    else:
        context.exception_raised = False


@when('I read the backup plan with id="{backup_plan_id}"')
def when_read_backup_plan_with_id(context, backup_plan_id):
    try:
        context.backup_plan_service = context.injector.get(BackupPlanService)
        context.backup_plan = context.backup_plan_service.get_by_id(UUID(backup_plan_id))
    except QueryException as e:
        context.exception_raised = True
    else:
        context.exception_raised = False


@then('it should be created successfully')
def backup_plan_has_added(context):
    assert context.exception_raised == False
    assert context.backup_plan is not None

    assert context.backup_plan.label == context.label
    assert context.backup_plan.source == Source(context.source_path)

    assert compare_destinations(
        context.backup_plan.destinations,
        context.destinations
    )


@then('the result should be "{result}"')
def then_cli_executed_successfully(context, result):
    match result:
        case "success":
            assert context.exception_raised == False
        case "error":
            assert context.exception_raised


@then('the result value should be "{result}"')
def then_cli_executed_successfully(context, result):
    assert result == str(context.result_value)


@then('the backup_plan must have same field: "{fields_json}"')
def backup_plan_has_added(context, fields_json):
    assert compare_backup_plan_fields(context.backup_plan, json_to_dict(fields_json))

