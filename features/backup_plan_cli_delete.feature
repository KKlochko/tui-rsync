Feature: Deleting a backup plan with the CLI

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Deleting a backup plan with the CLI
    Given the CLI arguments are "<arguments>"
    And I have a backup plan with id="<existing_backup_plan_id>"
    When I run the CLI
    Then the CLI executed with "<result>"

    Examples:
      | arguments                                                | existing_backup_plan_id              | result  | description                |
      | plans remove one -i 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | success | delete an existing plan    |
      | plans remove one -i 8aa59e7e-dc75-459b-aeb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be512 | error   | delete a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.cli
  Scenario Outline: Response of deleting non-existing backup plan with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI contains the error: "<result_error>"

    Examples:
      | arguments                                                | result | result_error                                                        | description                |
      | plans remove one -i 8aa59e7e-dc75-459b-aeb5-b710b39be583 | error  | [ERROR] Failed to delete the backup plan, because it doesn't exist. | delete a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Response of deleting existing backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    And I have a backup plan with id="<existing_backup_plan_id>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output contains "<result_message>"
    And the CLI output contains "<existing_backup_plan_id>"

    Examples:
      | arguments                                                | existing_backup_plan_id              | result  | result_message               | description            |
      | plans remove one -i 8aa59e7e-dc75-459b-aeb5-b710b39be583 | 8aa59e7e-dc75-459b-aeb5-b710b39be583 | success | Removed the backup plan with | delete the backup plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.cli
  Scenario Outline: Response of deleting no backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output contains "<result_message>"

    Examples:
      | arguments        | result  | result_message                      | description     |
      | plans remove all | success | Nothing to remove. No backup plans. | delete no plans |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Response of deleting all backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output contains "<result_message>"

    Examples:
      | arguments        | result  | result_message            | description             |
      | plans remove all | success | All backup plans removed. | delete all backup plans |
