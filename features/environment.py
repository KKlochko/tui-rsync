from behave import fixture, use_fixture
from injector import Injector
from typer.testing import CliRunner

from features.support import DataSeeds
from tui_rsync.core.components.backup_plan.application.repository.backup_plan_repository import BackupPlanRepository
from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from tui_rsync.infrastructure.configuration import TestingConfiguration, CurrentConfiguration
from tui_rsync.infrastructure.orm import InMemoryDatabaseManager
from tui_rsync.user_interface.cli.cli import cli_app


@fixture
def setup_cli_runner(context):
    context.cli_runner = CliRunner()
    yield context.cli_runner


@fixture
def setup_cli_app(context):
    context.cli_app = cli_app
    yield context.cli_app


@fixture
def setup_in_memory_database_manager(context):
    context.db = context.injector.get(InMemoryDatabaseManager)
    yield context.db


@fixture
def setup_backup_plan_service(context):
    context.backup_plan_service = context.injector.get(BackupPlanService)
    yield context.backup_plan_service


@fixture
def setup_injector_for_testing(context):
    context.injector = Injector([TestingConfiguration()])
    CurrentConfiguration.set_injector(context.injector)
    yield context.injector


@fixture
def setup_seeds(context):
    context.data_seeds = DataSeeds(context.injector)
    context.data_seeds.seeds(context)
    yield context.data_seeds


def before_tag(context, tag):
    if tag == "fixture.injector":
        use_fixture(setup_injector_for_testing, context)
    if tag == "fixture.in_memory_database":
        use_fixture(setup_in_memory_database_manager, context)
    if tag == "fixture.backup_plan_service":
        use_fixture(setup_backup_plan_service, context)
    if tag == "fixture.cli":
        use_fixture(setup_cli_app, context)
        use_fixture(setup_cli_runner, context)
    if tag == "fixture.seeds":
        use_fixture(setup_seeds, context)
