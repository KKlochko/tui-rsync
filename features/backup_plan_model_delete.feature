Feature: Delete a backup plan

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  Scenario Outline: Deleting a backup plan
    Given I have a backup plan with id="<existing_backup_plan_id>"
    When I remove the backup plan with id="<backup_plan_id>"
    Then the result should be "<result>"

    Examples:
      | backup_plan_id                       | existing_backup_plan_id              | result  | description                |
      | 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | success | delete an existing plan    |
      | 8aa59e7e-dc75-459b-aeb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be512 | error   | delete a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  Scenario: Delete no backup plans
    When I remove all backup plans
    Then the result value should be "False"

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  Scenario: Delete all backup plans
    Given I have a backup plan with id="8aa59e7e-dc75-459b-beb5-b710b39be583"
    When I remove all backup plans
    Then the result value should be "True"
