Feature: Update a backup plan

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  Scenario Outline: Update a backup plan's fields
    Given I have a backup plan with id="<existing_backup_plan_id>"
    When I update the backup plan with id="<backup_plan_id>" and data="<updated_data>"
    Then the result should be "<result>"

    Examples:
      | backup_plan_id                       | existing_backup_plan_id              | updated_data           | result  | description                |
      | 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | {"label": "new_label"} | success | update an existing plan    |
      | 8aa59e7e-dc75-459b-aeb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be512 | {"label": "new_label"} | error   | update a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  Scenario Outline: Updated backup plan's fields must be updated
    Given I have a backup plan with id="<existing_backup_plan_id>"
    When I update the backup plan with id="<backup_plan_id>" and data="<updated_data>"
    And I read the backup plan with id="<backup_plan_id>"
    Then the result should be "<result>"
    And the backup_plan must have same field: "<updated_data>"

    Examples:
      | backup_plan_id                       | existing_backup_plan_id              | updated_data                         | result  | description             |
      | 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | {"label": "new_label"}               | success | update the label        |
      | 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | {"source": "/new/mnt"}               | success | update the source       |
      | 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | {"destinations": ["/mnt2", "/mnt3"]} | success | update the destinations |
