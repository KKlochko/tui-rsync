Feature: Show a backup plan with the CLI

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Show a backup plan with the CLI
    Given the CLI arguments are "<arguments>"
    And I have a backup plan with id="<existing_backup_plan_id>"
    When I run the CLI
    Then the CLI executed with "<result>"

    Examples:
      | arguments                                              | existing_backup_plan_id              | result  | description              |
      | plans show one -i 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | success | show an existing plan    |
      | plans show one -i 8aa59e7e-dc75-459b-aeb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be512 | error   | show a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Response of showing existing backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    And I have a backup plan with id="<existing_backup_plan_id>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output doesn't contains "<result_message>"
    And the CLI output contains "<existing_backup_plan_id>"

    Examples:
      | arguments                                              | existing_backup_plan_id              | result  | result_message               | description            |
      | plans show one -i 8aa59e7e-dc75-459b-aeb5-b710b39be583 | 8aa59e7e-dc75-459b-aeb5-b710b39be583 | success | Removed the backup plan with | delete the backup plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.cli
  Scenario Outline: Response of showing non-existing backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI contains the error: "<result_error>"

    Examples:
      | arguments                                              | result | result_error                           | description              |
      | plans show one -i 8aa59e7e-dc75-459b-aeb5-b710b39be583 | error  | [ERROR] The backup plan was not found. | show a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.cli
  Scenario Outline: Response of showing no backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output contains "<result_message>"

    Examples:
      | arguments      | result  | result_message   | description    |
      | plans show all | success | No backup plans. | shows no plans |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Response of showing backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    And I have a backup plan with id="8aa59e7e-dc75-459b-beb5-b710b39be583"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output contains "8aa59e7e-dc75-459b-beb5-b710b39be583"
    And the CLI output doesn't contains "<no_message>"

    Examples:
      | arguments      | result  | no_message       | description |
      | plans show all | success | No backup plans. | shows plans |

