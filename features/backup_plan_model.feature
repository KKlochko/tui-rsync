Feature: Creating a backup plan

  @fixture.injector
  @fixture.in_memory_database
  @fixture.backup_plan_service
  Scenario Outline: Create an new unique backup plan
    Given the label "<label>"
    And the path "<source_path>"
    And the destinations <destinations>
    When I create the backup plan
    Then it should be created successfully

    Examples:
      | label | source_path | destinations                     |
      | usb   | /mnt/usb    | []                               |
      | db    | /db         | ["/backup/db"]                   |
      | temp  | /tmp        | ["/backup/tmp1", "/backup/tmp2"] |
