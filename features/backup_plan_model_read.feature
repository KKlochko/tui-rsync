Feature: Read a backup plan by an id

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  Scenario Outline: Read a backup plan with the CLI
    Given I have a backup plan with id="<existing_backup_plan_id>"
    When I read the backup plan with id="<backup_plan_id>"
    Then the result should be "<result>"

    Examples:
      | backup_plan_id                       | existing_backup_plan_id              | result  | description                |
      | 8aa59e7e-dc75-459b-beb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | success | delete an existing plan    |
      | 8aa59e7e-dc75-459b-aeb5-b710b39be583 | 8aa59e7e-dc75-459b-beb5-b710b39be512 | error   | delete a non-existing plan |
