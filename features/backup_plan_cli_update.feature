Feature: Update a backup plan with the CLI

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Replace a backup plan with the CLI
    Given the CLI arguments are "<arguments>"
    And I have a backup plan with id="<existing_backup_plan_id>"
    When I run the CLI
    Then the CLI executed with "<result>"

    Examples:
      | arguments                                                                                      | existing_backup_plan_id              | result  | description                 |
      | plans update replace -i 8aa59e7e-dc75-459b-beb5-b710b39be583 -l test -s /mnt -d /mnt2 -d /mnt3 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | success | replace an existing plan    |
      | plans update replace -i 8aa59e7e-dc75-459b-beb5-b710b39be583 -l test -s /mnt -d /mnt2 -d /mnt3 | 8aa59e7e-dc75-459b-beb5-b710b39be512 | error   | replace a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.cli
  Scenario Outline: Response of replacing non-existing backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI contains the error: "<result_error>"

    Examples:
      | arguments                                                                                      | result | result_error                           | description                 |
      | plans update replace -i 8aa59e7e-dc75-459b-beb5-b710b39be583 -l test -s /mnt -d /mnt2 -d /mnt3 | error  | [ERROR] The backup plan was not found. | replace a non-existing plan |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.seeds
  @fixture.cli
  Scenario Outline: Response of replacing an existing backup plans with the CLI
    Given the CLI arguments are "<arguments>"
    And I have a backup plan with id="<existing_backup_plan_id>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output contains "<result_message>"

    Examples:
      | arguments                                                                                      | existing_backup_plan_id              | result  | result_message           | description              |
      | plans update replace -i 8aa59e7e-dc75-459b-beb5-b710b39be583 -l test -s /mnt -d /mnt2 -d /mnt3 | 8aa59e7e-dc75-459b-beb5-b710b39be583 | success | The backup plan updated. | replace an existing plan |

