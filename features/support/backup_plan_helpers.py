from features.support.helpers import json_to_dict
from tui_rsync.core.components.backup_plan.domain import Destination, BackupPlan, Source


def json_to_backup_plan(backup_plan_json):
    fields = json_to_dict(backup_plan_json)

    backup_plan = BackupPlan(
        label=fields['label'],
        source=Source(fields['source']),
        destinations=list(map(lambda path: Destination(path), fields['destinations'])),
    )

    return backup_plan


def update_backup_plan(backup_plan, fields: dict):
    for field, value in fields.items():
        match field:
            case 'label':
                backup_plan.label = value
            case 'source':
                backup_plan.source = Source(value)
            case 'destinations':
                backup_plan.destinations = list(map(lambda path: Destination(path), value))

    return backup_plan


def compare_backup_plan_fields(backup_plan, fields: dict):
    for field, value in fields.items():
        match field:
            case 'label':
                if backup_plan.label != value:
                    return False
            case 'source':
                if backup_plan.source.path != value:
                    return False
            case 'destinations':
                if not compare_destinations(backup_plan.destinations, value):
                    return False

    return True


def compare_destinations(actual: list[Destination], expected: list[str]) -> bool:
    actual_path_set = {destionation.path for destionation in actual}
    return actual_path_set == set(expected)


def compare_backup_plans(backup_plan, expected):
    return backup_plan.label == expected.label \
           and backup_plan.source == expected.source_path \
           and compare_destinations(
                   backup_plan.destinations,
                   expected.destinations
               )
