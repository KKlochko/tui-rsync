from injector import Injector

from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from . import FakeFactories


class DataSeeds:
    def __init__(self, configuration: Injector):
        self.fake_factories = FakeFactories()
        self.configuration = configuration

    def create_backup_plan(self, uuid: str | None = None):
        service = self.configuration.get(BackupPlanService)
        backup_plan = self.fake_factories.backup_plan(uuid)
        service.add(backup_plan)
        return backup_plan

    def seeds(self, context):
        context.backup_plan_seed = self.create_backup_plan()
