from faker import Faker

from tui_rsync.core.components.backup_plan.domain import BackupPlan, Path, BackupPlanId, Source, Destination
from tui_rsync.core.shared_kernel.components.common import UUID, Label


class FakeFactories:
    def __init__(self):
        self.faker = Faker()

    def uuid(self):
        return UUID(self.faker.uuid4())

    def backup_plan_id(self):
        return BackupPlanId(self.uuid().id)

    def label(self) -> Label:
        return Label(self.faker.sentence(nb_words=4))

    def path(self) -> Path:
        return Path(self.faker.file_path())

    def source(self) -> Source:
        return Source(self.path().path)

    def destination(self) -> Destination:
        return Destination(self.path().path)

    def backup_plan(self, uuid: str | None = None):
        uuid = self.backup_plan_id() if uuid is None else UUID(uuid)
        return BackupPlan(id=uuid,
                          label=self.label(),
                          source=self.source(),
                          destinations=[self.destination()])
