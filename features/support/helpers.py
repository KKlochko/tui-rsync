import json


def json_to_dict(json_data):
    return json.loads(json_data)
