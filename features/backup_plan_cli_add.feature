Feature: Creating a backup plan with the CLI

  @fixture.injector
  @fixture.in_memory_database
  @fixture.cli
  Scenario Outline: Create an new unique backup plan with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "success"

    Examples:
      | arguments                                                  |
      | plans add --label my_label --source /mnt -d /mnt2 -d /mnt3 |
      | plans add --label label2   --source /mnt -d /mnt2 -d /mnt3 |

  @fixture.injector
  @fixture.in_memory_database
  @fixture.cli
  Scenario Outline: Response of creating an new unique backup plan with the CLI
    Given the CLI arguments are "<arguments>"
    When I run the CLI
    Then the CLI executed with "<result>"
    And the CLI output contains "<result_message>"

    Examples:
      | arguments                                                  | result  | result_message            | description |
      | plans add --label my_label --source /mnt -d /mnt2 -d /mnt3 | success | The backup plan is added. | add a plan  |

