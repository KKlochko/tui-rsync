from injector import Injector

from tui_rsync.infrastructure.configuration import UserDataPaths
from tui_rsync.infrastructure.configuration import Configuration, CurrentConfiguration
from tui_rsync.user_interface.cli.cli import cli_app


def main():
    injector = Injector([Configuration()])
    CurrentConfiguration.set_injector(injector)
    UserDataPaths().safe_create_user_data_dir()
    cli_app()

if __name__ == "__main__":
    main()
