from abc import ABC, abstractmethod


class DatabaseManagerPort(ABC):
    @abstractmethod
    def get_connection(self):
        pass

    @abstractmethod
    def create_tables(self):
        pass
