from abc import ABC, abstractmethod


class UserDataPathsPort(ABC):
    @abstractmethod
    def safe_create_user_data_dir(self):
        pass

    @abstractmethod
    def get_user_db_path(self):
        pass

    @abstractmethod
    def get_user_data_dir(self):
        pass
