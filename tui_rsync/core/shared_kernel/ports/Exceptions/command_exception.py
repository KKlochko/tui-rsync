from . import AppException


class CommandException(AppException):
    """Command failed to change persistence data."""
    pass
