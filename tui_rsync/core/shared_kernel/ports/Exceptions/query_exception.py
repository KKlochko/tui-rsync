from . import AppException


class QueryException(AppException):
    """Query failed."""
    pass
