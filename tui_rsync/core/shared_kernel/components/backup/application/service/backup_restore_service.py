from tui_rsync.core.components.backup.application.backup_commands import BackupSyncCommand, BackupSyncCommandDryRun
from tui_rsync.core.components.backup_plan.application.repository import BackupPlanRepositoryPort
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.user_interface.cli.shared_kernel.components.prompts.applications.prompts import ChoosePromptPort


class BackupRestoreService:
    def __init__(self, backup_plan_repository: BackupPlanRepositoryPort):
        self.backup_plan_repository = backup_plan_repository

    def sync_by_plan_id(self, uuid: UUID, choose_prompt: ChoosePromptPort, args: str = '', dry_run: bool = False):
        backup_plan = self.backup_plan_repository.get_by_id(uuid)

        destinations = (destination.path for destination in backup_plan.destinations)
        destination = choose_prompt.choose(destinations)

        if dry_run:
            backup_command = BackupSyncCommandDryRun(destination, backup_plan.source.path, args)
            backup_command.run()
        else:
            backup_command = BackupSyncCommand(destination, backup_plan.source.path, args)
            backup_command.run()
