from tui_rsync.core.components.backup.application.backup_commands import BackupSyncCommand, BackupSyncCommandDryRun
from tui_rsync.core.components.backup_plan.application.repository import BackupPlanRepositoryPort
from tui_rsync.core.shared_kernel.components.common import UUID


class BackupSyncService:
    def __init__(self, backup_plan_repository: BackupPlanRepositoryPort):
        self.backup_plan_repository = backup_plan_repository

    def sync_by_plan_id(self, uuid: UUID, args: str = '', dry_run: bool = False):
        backup_plan = self.backup_plan_repository.get_by_id(uuid)

        if dry_run:
            for destination in backup_plan.destinations:
                backup_command = BackupSyncCommandDryRun(backup_plan.source.path, destination.path, args)
                backup_command.run()
        else:
            for destination in backup_plan.destinations:
                backup_command = BackupSyncCommand(backup_plan.source.path, destination.path, args)
                backup_command.run()
