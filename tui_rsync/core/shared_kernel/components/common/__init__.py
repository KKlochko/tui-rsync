from .domain import UUID, Label

__all__ = ['UUID', 'Label']
