from .value_objects import UUID, Label

__all__ = ['UUID', 'Label']
