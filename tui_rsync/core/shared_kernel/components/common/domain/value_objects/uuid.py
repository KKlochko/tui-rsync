from dataclasses import dataclass, field
import uuid


@dataclass(frozen=True)
class UUID:
    id: str = field(default_factory=lambda: UUID.generate().id)

    @classmethod
    def generate(cls) -> 'UUID':
        return cls(str(uuid.uuid4()))

    def __str__(self):
        return self.id