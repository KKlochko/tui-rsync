from dataclasses import dataclass, field


@dataclass(frozen=True)
class Label:
    label: str = field(default='')
