from abc import ABC, abstractmethod


class BackupCommandPort(ABC):
    @abstractmethod
    def run(self):
        pass
