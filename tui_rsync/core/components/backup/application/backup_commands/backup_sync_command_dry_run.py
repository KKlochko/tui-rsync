import shlex
from subprocess import Popen, PIPE

from . import BackupCommandPort


class BackupSyncCommandDryRun(BackupCommandPort):
    def __init__(self, source: str, destination: str, args: str):
        self._args = ["rsync"] + shlex.split(args) + ['--dry-run', source, destination]

    def run(self):
        output = Popen(self._args, stdout=PIPE)
        return output.communicate()

