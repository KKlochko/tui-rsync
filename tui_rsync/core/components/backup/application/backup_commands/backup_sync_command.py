import shlex
from subprocess import Popen, PIPE

from . import BackupCommandPort


class BackupSyncCommand(BackupCommandPort):
    def __init__(self, source: str, destination: str, args: str):
        self._args = ["rsync"] + shlex.split(args) + [source, destination]

    def run(self):
        output = Popen(self._args, stdout=PIPE)
        response = output.communicate()

