from tui_rsync.core.ports.orm import DatabaseManagerPort
from tui_rsync.infrastructure.orm.dto.dtos import BackupPlanDTO

from tui_rsync.infrastructure.orm.models import BackupPlanModel


class GetAllBackupPlansQuery:
    def __init__(self, database_manager: DatabaseManagerPort):
        self.databaseManager = database_manager

    def execute(self):
        return (BackupPlanDTO.to_domain(model) for model in BackupPlanModel.select().iterator())
