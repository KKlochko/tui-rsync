from tui_rsync.core.components.backup_plan.domain import BackupPlan
from tui_rsync.core.ports.orm import DatabaseManagerPort
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.core.shared_kernel.ports.Exceptions.query_exception import QueryException
from tui_rsync.infrastructure.orm.dto.dtos import BackupPlanDTO

from tui_rsync.infrastructure.orm.models import BackupPlanModel


class GetBackupPlanByIdQuery:
    def __init__(self, database_manager: DatabaseManagerPort):
        self.databaseManager = database_manager

    def execute(self, uuid: UUID) -> BackupPlan:
        model = BackupPlanModel.get_or_none(BackupPlanModel.id == uuid.id)

        if model is None:
            raise QueryException("The backup plan was not found.")

        return BackupPlanDTO.to_domain(model)
