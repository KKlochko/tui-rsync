from tui_rsync.core.ports.orm import DatabaseManagerPort

from tui_rsync.infrastructure.orm.models import BackupPlanModel


class GetBackupPlanCountQuery:
    def __init__(self, database_manager: DatabaseManagerPort):
        self.databaseManager = database_manager

    def execute(self):
        return BackupPlanModel.select().count()
