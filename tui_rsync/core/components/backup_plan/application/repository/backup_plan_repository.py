from typing import Optional

from tui_rsync.core.ports.orm import DatabaseManagerPort
from tui_rsync.infrastructure.orm.dto.dtos import BackupPlanDTO
from .backup_plan_repository_port import BackupPlanRepositoryPort

from tui_rsync.core.components.backup_plan.domain import BackupPlan
from tui_rsync.core.shared_kernel.components.common import UUID

from tui_rsync.infrastructure.orm.models import BackupPlanModel
from ..commands import RemoveBackupPlanDestinationsCommand, RemoveBackupPlanCommand
from ..queries import GetBackupPlanByIdQuery


class BackupPlanRepository(BackupPlanRepositoryPort):
    def __init__(self, database_manager: DatabaseManagerPort):
        self.databaseManager = database_manager

    def add(self, backup_plan: BackupPlan):
        model = BackupPlanDTO.to_model(backup_plan)

        model.save(force_insert=True)
        for destination in model.destinations:
            destination.save(force_insert=True)

    def get_by_id(self, uuid: UUID) -> BackupPlan:
        query = GetBackupPlanByIdQuery(self.databaseManager)
        return query.execute(uuid)

    def update(self, backup_plan: BackupPlan):
        updated_model = BackupPlanDTO.to_model(backup_plan)
        query = GetBackupPlanByIdQuery(self.databaseManager)

        old_plan = query.execute(backup_plan.id)
        model = BackupPlanDTO.to_model(old_plan)
        remove_backup_plan_destinations_command = RemoveBackupPlanDestinationsCommand(self.databaseManager)

        model.label = updated_model.label
        model.source = updated_model.source

        remove_backup_plan_destinations_command.execute(backup_plan.id)

        model.save()
        for destination in updated_model.destinations:
            destination.save(force_insert=True)

    def delete(self, uuid: UUID) -> bool:
        command = RemoveBackupPlanCommand(self.databaseManager)
        return command.execute(uuid)
