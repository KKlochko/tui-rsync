from abc import ABC, abstractmethod
from typing import Optional

from tui_rsync.core.components.backup_plan.domain import BackupPlan
from tui_rsync.core.shared_kernel.components.common import UUID


class BackupPlanRepositoryPort(ABC):
    @abstractmethod
    def add(self, backup_plan: BackupPlan):
        pass

    @abstractmethod
    def get_by_id(self, uuid: UUID) -> Optional[BackupPlan]:
        pass

    @abstractmethod
    def update(self, backup_plan: BackupPlan):
        pass

    @abstractmethod
    def delete(self, uuid: UUID) -> bool:
        pass
