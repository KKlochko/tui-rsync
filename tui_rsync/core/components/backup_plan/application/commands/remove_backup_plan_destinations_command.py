from tui_rsync.core.ports.orm import DatabaseManagerPort
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.infrastructure.orm.models import DestinationModel


class RemoveBackupPlanDestinationsCommand:
    def __init__(self, database_manager: DatabaseManagerPort):
        self.databaseManager = database_manager

    def execute(self, backup_plan_uuid: UUID) -> bool:
        rows = DestinationModel.delete().where(DestinationModel.source == backup_plan_uuid.id).execute()
        return rows != 0
