from tui_rsync.core.ports.orm import DatabaseManagerPort
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.core.shared_kernel.ports.Exceptions import CommandException
from tui_rsync.infrastructure.orm.models import DestinationModel, BackupPlanModel


class RemoveBackupPlanCommand:
    def __init__(self, database_manager: DatabaseManagerPort):
        self.databaseManager = database_manager

    def execute(self, uuid: UUID) -> bool:
        deleted = BackupPlanModel.delete().where(BackupPlanModel.id == uuid.id).execute()
        deleted = DestinationModel.delete().where(DestinationModel.source == uuid.id).execute() | deleted

        if deleted == 0:
            raise CommandException("Failed to delete the backup plan, because it doesn't exist.")

        return True
