from tui_rsync.core.ports.orm import DatabaseManagerPort
from tui_rsync.infrastructure.orm.models import BackupPlanModel, DestinationModel


class RemoveAllBackupPlansCommand:
    def __init__(self, database_manager: DatabaseManagerPort):
        self.databaseManager = database_manager

    def execute(self) -> bool:
        rows = BackupPlanModel.delete().execute()
        rows = DestinationModel.delete().execute() + rows
        return rows != 0
