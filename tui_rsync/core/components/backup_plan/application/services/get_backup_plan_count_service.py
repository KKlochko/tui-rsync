from typing import List

from tui_rsync.core.components.backup_plan.application.queries import GetBackupPlanCountQuery

from tui_rsync.core.components.backup_plan.domain import BackupPlan


class GetBackupPlanCountService:
    def __init__(self, get_backup_plan_count_query: GetBackupPlanCountQuery):
        self.get_backup_plan_count_query = get_backup_plan_count_query

    def count(self) -> int:
        return self.get_backup_plan_count_query.execute()

    def is_empty(self) -> bool:
        return self.count() == 0
