from typing import Optional

from tui_rsync.core.components.backup_plan.application.repository import BackupPlanRepositoryPort

from tui_rsync.core.components.backup_plan.domain import BackupPlan
from tui_rsync.core.shared_kernel.components.common import UUID


class BackupPlanService:
    def __init__(self, backup_plan_repository: BackupPlanRepositoryPort):
        self.backup_plan_repository = backup_plan_repository

    def add(self, backup_plan: BackupPlan):
        self.backup_plan_repository.add(backup_plan)

    def get_by_id(self, uuid: UUID) -> Optional[BackupPlan]:
        return self.backup_plan_repository.get_by_id(uuid)

    def update(self, backup_plan: BackupPlan):
        return self.backup_plan_repository.update(backup_plan)

    def delete(self, uuid: UUID) -> bool:
        return self.backup_plan_repository.delete(uuid)
