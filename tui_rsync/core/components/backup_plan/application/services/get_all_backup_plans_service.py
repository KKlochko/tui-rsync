from typing import List

from tui_rsync.core.components.backup_plan.application.queries import GetAllBackupPlansQuery

from tui_rsync.core.components.backup_plan.domain import BackupPlan


class GetAllBackupPlansService:
    def __init__(self, get_all_backup_plan_query: GetAllBackupPlansQuery):
        self.get_all_backup_plan_query = get_all_backup_plan_query

    def get_all(self) -> List[BackupPlan]:
        return self.get_all_backup_plan_query.execute()
