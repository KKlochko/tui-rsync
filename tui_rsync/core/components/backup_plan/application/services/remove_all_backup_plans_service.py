from tui_rsync.core.components.backup_plan.application.commands import RemoveAllBackupPlansCommand


class RemoveAllBackupPlansService:
    def __init__(self, remove_all_backup_plan_command: RemoveAllBackupPlansCommand):
        self.remove_all_backup_plan_command = remove_all_backup_plan_command

    def remove_all(self) -> bool:
        return self.remove_all_backup_plan_command.execute()
