from tui_rsync.core.shared_kernel.components.common.domain import UUID


class BackupPlanId(UUID):
    pass
