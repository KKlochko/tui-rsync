from dataclasses import dataclass


@dataclass
class Path:
    path: str
