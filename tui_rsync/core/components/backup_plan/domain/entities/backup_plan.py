from dataclasses import dataclass, field
from typing import List

from ..value_objects import BackupPlanId, Source, Destination
from tui_rsync.core.shared_kernel.components.common.domain import Label


@dataclass
class BackupPlan:
    source: Source
    destinations: List[Destination]

    id: BackupPlanId = field(default_factory=lambda: BackupPlanId.generate())
    label: Label = field(default='')
