from injector import singleton, Module, provider

from tui_rsync.core.components.backup_plan.application.commands import RemoveAllBackupPlansCommand
from tui_rsync.core.components.backup_plan.application.queries import GetAllBackupPlansQuery, GetBackupPlanCountQuery
from tui_rsync.core.components.backup_plan.application.repository import BackupPlanRepositoryPort, BackupPlanRepository
from tui_rsync.core.components.backup_plan.application.services import GetBackupPlanCountService, \
    GetAllBackupPlansService, RemoveAllBackupPlansService
from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from tui_rsync.core.ports.configuration import UserDataPathsPort
from tui_rsync.core.ports.orm import DatabaseManagerPort
from tui_rsync.core.shared_kernel.components.backup.application.service import BackupSyncService
from tui_rsync.core.shared_kernel.components.backup.application.service.backup_restore_service import \
    BackupRestoreService
from tui_rsync.infrastructure.configuration import UserDataPaths
from tui_rsync.infrastructure.orm import InMemoryDatabaseManager
from tui_rsync.user_interface.cli.shared_kernel.components.prompts.applications.prompts import ChoosePromptFzf


class TestingConfiguration(Module):
    @provider
    @singleton
    def provide_user_data_paths(self) -> UserDataPathsPort:
        return UserDataPaths()

    @provider
    @singleton
    def provide_database_manager(self) -> DatabaseManagerPort:
        return InMemoryDatabaseManager()

    @provider
    @singleton
    def provide_backup_plan_repository(self, database_manager: DatabaseManagerPort) -> BackupPlanRepositoryPort:
        return BackupPlanRepository(database_manager)

    @provider
    @singleton
    def provide_backup_plan_service(self, backup_plan_repository: BackupPlanRepositoryPort) -> BackupPlanService:
        return BackupPlanService(backup_plan_repository)

    @provider
    @singleton
    def provide_remove_all_backup_plans_command(self, database_manager: DatabaseManagerPort) -> RemoveAllBackupPlansCommand:
        return RemoveAllBackupPlansCommand(database_manager)

    @provider
    @singleton
    def provide_remove_all_backup_plans_service(self, command: RemoveAllBackupPlansCommand) -> RemoveAllBackupPlansService:
        return RemoveAllBackupPlansService(command)

    @provider
    @singleton
    def provide_get_all_backup_plans_query(self, database_manager: DatabaseManagerPort) -> GetAllBackupPlansQuery:
        return GetAllBackupPlansQuery(database_manager)

    @provider
    @singleton
    def provide_get_backup_plan_count_query(self, database_manager: DatabaseManagerPort) -> GetBackupPlanCountQuery:
        return GetBackupPlanCountQuery(database_manager)

    @provider
    @singleton
    def provide_get_backup_plan_count_service(self, query: GetBackupPlanCountQuery) -> GetBackupPlanCountService:
        return GetBackupPlanCountService(query)

    @provider
    @singleton
    def provide_get_all_backup_plans_service(self, query: GetAllBackupPlansQuery) -> GetAllBackupPlansService:
        return GetAllBackupPlansService(query)

    @provider
    @singleton
    def provide_backup_sync_service(self, backup_plan_repository: BackupPlanRepositoryPort) -> BackupSyncService:
        return BackupSyncService(backup_plan_repository)

    @provider
    @singleton
    def provide_backup_restore_service(self, backup_plan_repository: BackupPlanRepositoryPort) -> BackupRestoreService:
        return BackupRestoreService(backup_plan_repository)

    @provider
    @singleton
    def provide_choose_prompt_fzf(self) -> ChoosePromptFzf:
        return ChoosePromptFzf()
