################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################

import platformdirs
import os

from tui_rsync.core.ports.configuration import UserDataPathsPort


class UserDataPaths(UserDataPathsPort):
    """
    Configuration of the tui-rsync
    """
    __APP_NAME = "tui-rsync"
    __APP_AUTHOR = "KKlochko"
    __DB_NAME = "sync.db"

    def safe_create_user_data_dir(self):
        self.safe_create_path(self.get_user_data_dir())

    def get_user_db_path(self):
        return platformdirs.user_data_path(
            self.__APP_NAME,
            self.__APP_AUTHOR,
            self.__DB_NAME
        )

    def get_user_data_dir(self):
        return platformdirs.user_data_dir(
            self.__APP_NAME,
            self.__APP_AUTHOR,
        )

    @staticmethod
    def safe_create_path(path):
        """
        Create path's folders if they do not exist
        """
        if not os.path.exists(path):
            os.makedirs(path)
