from injector import Injector


class CurrentConfiguration:
    """
    Manages global state for the configuration using injector.
    """

    _injector: Injector | None = None

    @staticmethod
    def get_injector() -> Injector:
        if CurrentConfiguration._injector is None:
            raise RuntimeError("Injector not initialized")
        return CurrentConfiguration._injector

    @staticmethod
    def get(instance):
        return CurrentConfiguration.get_injector().get(instance)

    @staticmethod
    def set_injector(new_injector):
        CurrentConfiguration._injector = new_injector
