from typing import List
from peewee import Model, Database
from .models import BackupPlanModel, DestinationModel
from tui_rsync.core.ports.orm import DatabaseManagerPort


class DatabaseManager(DatabaseManagerPort):
    models: List[Model] = [
        BackupPlanModel,
        DestinationModel,
    ]

    def __init__(self, db: Database):
        self.db = db
        self._update_model_meta()
        self.create_tables()

    def _update_model_meta(self):
        for model in self.models:
            model._meta.database = self.db

    def get_connection(self):
        return self.db

    def create_tables(self):
        self.db.create_tables(self.models, safe=True)
