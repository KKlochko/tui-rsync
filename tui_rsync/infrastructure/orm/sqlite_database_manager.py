from peewee import SqliteDatabase
from .database_manager import DatabaseManager
from ...core.ports.configuration import UserDataPathsPort


class SqliteDatabaseManager(DatabaseManager):
    def __init__(self, user_data_paths: UserDataPathsPort):
        filepath = user_data_paths.get_user_db_path()
        super().__init__(SqliteDatabase(filepath))
