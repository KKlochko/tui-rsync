from peewee import *

from tui_rsync.core.components.backup_plan.domain import BackupPlan, Destination, Source, BackupPlanId
from tui_rsync.infrastructure.orm.models import BackupPlanModel, DestinationModel


class BackupPlanDTO:
    @staticmethod
    def to_domain(model: BackupPlanModel) -> BackupPlan:
        return BackupPlan(
            id=BackupPlanId(model.id),
            label=model.label,
            source=Source(model.source),
            destinations=list(map(lambda destination: Destination(destination.path), model.destinations)),
        )

    @staticmethod
    def to_model(entity: BackupPlan) -> BackupPlanModel:
        model = BackupPlanModel(
            id=entity.id.id,
            label=entity.label,
            source=entity.source.path,
            destinations=[],
        )

        for destination in entity.destinations:
            model.destinations.append(
                DestinationModel(
                    source=model,
                    path=destination.path
                )
            )

        return model
