from peewee import *
import uuid


class BackupPlanModel(Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4)
    label = CharField()
    source = CharField()

    class Meta:
        database = DatabaseProxy()


class DestinationModel(Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4)
    source = ForeignKeyField(BackupPlanModel, backref='destinations')
    path = CharField()
