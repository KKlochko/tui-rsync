from peewee import SqliteDatabase
from .database_manager import DatabaseManager


class InMemoryDatabaseManager(DatabaseManager):
    def __init__(self):
        super().__init__(SqliteDatabase(':memory:'))
