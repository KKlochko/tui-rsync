from abc import ABC, abstractmethod


class ChoosePromptPort(ABC):
    @staticmethod
    @abstractmethod
    def choose(iterator) -> str:
        pass
