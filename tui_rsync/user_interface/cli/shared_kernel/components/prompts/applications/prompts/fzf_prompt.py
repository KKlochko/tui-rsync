from .choose_prompt_port import ChoosePromptPort
from pyfzf import FzfPrompt


class ChoosePromptFzf(ChoosePromptPort):
    @staticmethod
    def choose(iterator) -> str:
        fzf = FzfPrompt()
        return fzf.prompt(iterator)[0]
