################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################

from rich.console import Console
from typing import List, Optional
import typer

from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from tui_rsync.core.components.backup_plan.domain import BackupPlan, Source, Destination
from tui_rsync.core.shared_kernel.components.common import Label
from tui_rsync.infrastructure.configuration import CurrentConfiguration


console = Console()
add_backup_plan = typer.Typer()


@add_backup_plan.command()
def add(
    label: str = typer.Option(
        '', "--label", "-l",
        help="[b]The label[/] is a uniq identification of a [b]source[/].",
        show_default=False
    ),
    source: str = typer.Option(
        None, "--source", "-s",
        help="[b]A source[/] of the data.",
        show_default=False
    ),
    destinations: Optional[List[str]] = typer.Option(
        None, "--destination", "-d", help="[b]The backup[/] destinations.",
        show_default=False
    )
):
    """
    [green b]Create[/] a [yellow]new backup plan[/] with a [bold]uniq[/] label.
    [b]The source[/] will be connected to [b]backup destinations[/].
    """

    if source is None:
        source = console.input("What is the [yellow b]path to the source[/]? ")

    service: BackupPlanService = CurrentConfiguration.get(BackupPlanService)

    plan = BackupPlan(label=Label(label), source=Source(source), destinations=[Destination(path) for path in destinations])
    service.add(plan)

    console.print("The backup plan is added.")
