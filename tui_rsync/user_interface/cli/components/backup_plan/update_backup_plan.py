################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################

from rich.console import Console
from typing import List, Optional
import typer

from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from tui_rsync.core.components.backup_plan.domain import BackupPlan, Source, Destination, BackupPlanId
from tui_rsync.core.shared_kernel.components.common import Label
from tui_rsync.core.shared_kernel.ports.Exceptions import CommandException
from tui_rsync.core.shared_kernel.ports.Exceptions.query_exception import QueryException
from tui_rsync.infrastructure.configuration import CurrentConfiguration
from tui_rsync.user_interface.cli.shared_kernel.components.errors.applications.exceptions import catch_exception

console = Console()
update_backup_plan = typer.Typer()


@update_backup_plan.command()
@catch_exception(CommandException, 1)
@catch_exception(QueryException, 1)
def replace(
    id: str = typer.Option(
        None, "--id", "-i",
        help="[b]The id[/] is an uniq identification of a [b]backup plan[/].",
        show_default=False
    ),
    label: str = typer.Option(
        '', "--label", "-l",
        help="[b]The label[/] is a uniq identification of a [b]source[/].",
        show_default=False
    ),
    source: str = typer.Option(
        None, "--source", "-s",
        help="[b]A source[/] of the data.",
        show_default=False
    ),
    destinations: Optional[List[str]] = typer.Option(
        None, "--destination", "-d", help="[b]The backup[/] destinations.",
        show_default=False
    )
):
    """
    [green b]Replace[/] a [yellow]backup plan[/] with a [bold]id[/] with [y]new data[/].
    """

    if source is None:
        source = console.input("What is the [yellow b]path to the source[/]? ")

    service: BackupPlanService = CurrentConfiguration.get(BackupPlanService)

    plan = BackupPlan(id=BackupPlanId(id), label=Label(label), source=Source(source), destinations=[Destination(path) for path in destinations])
    service.update(plan)

    console.print("The backup plan updated.")

