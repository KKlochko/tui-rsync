################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################


import typer
from .add_backup_plan import add_backup_plan
from .show_backup_plan import show_backup_plan
from .remove_backup_plan import remove_backup_plan
from .update_backup_plan import update_backup_plan


backup_plan = add_backup_plan
backup_plan.add_typer(show_backup_plan, name="show", help="Show backup plans")
backup_plan.add_typer(remove_backup_plan, name="remove", help="Remove backup plans")
backup_plan.add_typer(update_backup_plan, name="update", help="Update backup plans")
