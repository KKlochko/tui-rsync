################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################

from rich.console import Console
import typer

from tui_rsync.core.components.backup_plan.application.services import GetAllBackupPlansService, \
    GetBackupPlanCountService

from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.core.shared_kernel.ports.Exceptions.query_exception import QueryException
from tui_rsync.infrastructure.configuration import CurrentConfiguration
from tui_rsync.user_interface.cli.components.backup_plan.formating import BackupPlanFormat
from tui_rsync.user_interface.cli.shared_kernel.components.errors.applications.exceptions import catch_exception

console = Console()
show_backup_plan = typer.Typer()


@show_backup_plan.command()
@catch_exception(QueryException, 1)
def one(
    id: str = typer.Option(
        None, "--id", "-i",
        help="[b]The label[/] is a uniq identification of a [b]source[/].",
        show_default=False
    ),
):
    """
    [green b]Show[/] an [yellow]existing backup plan by the id[/].
    """

    service: BackupPlanService = CurrentConfiguration.get(BackupPlanService)

    plan = service.get_by_id(UUID(id))

    console.print(BackupPlanFormat.format(plan))


@show_backup_plan.command()
def all():
    """
    [green b]Show[/] [yellow]all existing backup plans[/].
    """

    count_service = CurrentConfiguration.get(GetBackupPlanCountService)
    if count_service.is_empty():
        console.print("No backup plans.")
    else:
        service = CurrentConfiguration.get(GetAllBackupPlansService)
        for backup_plan in service.get_all():
            console.print(BackupPlanFormat.format(backup_plan))
