################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################


from tui_rsync.core.components.backup_plan.domain import BackupPlan


class BackupPlanFormat:
    @staticmethod
    def format(backup_plan: BackupPlan, prefix='') -> str:
        output = f"[b]uuid:[/] {backup_plan.id.id}\n" \
                 f"[b]label:[/] {backup_plan.label}\n" \
                 f"[b]source:[/] {backup_plan.source.path}\n" \
                 f"[b]destionations:[/] \n"

        for destination in backup_plan.destinations:
            output += f"\t{destination.path}\n"

        if prefix != '':
            keepends = True
            output = prefix + f'{prefix}'.join(output.splitlines(keepends))

        return output
