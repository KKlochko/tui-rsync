################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################

from rich.console import Console
import typer

from tui_rsync.core.components.backup_plan.application.services import RemoveAllBackupPlansService
from tui_rsync.core.components.backup_plan.application.services.backup_plan_service import BackupPlanService
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.core.shared_kernel.ports.Exceptions import CommandException
from tui_rsync.infrastructure.configuration import CurrentConfiguration
from tui_rsync.user_interface.cli.shared_kernel.components.errors.applications.exceptions import catch_exception

console = Console()
remove_backup_plan = typer.Typer()


@remove_backup_plan.command()
@catch_exception(CommandException, 1)
def one(
    id: str = typer.Option(
        None, "--id", "-i",
        help="[b]The id[/] is an uniq identification of a [b]backup plan[/].",
        show_default=False
    )
):
    """
    [red b]Remove[/] an [yellow]existing backup plan[/].
    """

    service: BackupPlanService = CurrentConfiguration.get(BackupPlanService)

    service.delete(UUID(id))

    console.print(f"Removed the backup plan with {id}.")


@remove_backup_plan.command()
def all():
    """
    [red b]Remove[/] [yellow] all existing backup plans[/].
    """

    service: RemoveAllBackupPlansService = CurrentConfiguration.get(RemoveAllBackupPlansService)

    if service.remove_all():
        console.print("All backup plans removed.")
    else:
        console.print("Nothing to remove. No backup plans.")
