################################################################################
# Copyright (C) 2023-2025 Kostiantyn Klochko <kklochko@protonmail.com>         #
#                                                                              #
# This file is part of tui-rsync.                                              #
#                                                                              #
# tui-rsync is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# tui-rsync is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# tui-rsync. If not, see <https://www.gnu.org/licenses/>.                      #
################################################################################


from rich.console import Console
import typer

from tui_rsync.core.shared_kernel.components.backup.application.service import BackupSyncService
from tui_rsync.core.shared_kernel.components.backup.application.service.backup_restore_service import \
    BackupRestoreService
from tui_rsync.core.shared_kernel.components.common import UUID
from tui_rsync.infrastructure.configuration import CurrentConfiguration
from tui_rsync.user_interface.cli.shared_kernel.components.prompts.applications.prompts import ChoosePromptFzf

console = Console()
sync = typer.Typer()


@sync.command()
def one(
    id: str = typer.Option(
        None, "--id", "-i",
        help="[b]The id[/] is an uniq identification of a [b]backup plan[/].",
        show_default=False
    ),
    dry: bool = typer.Option(
        False, "-d", "--dry-run",
        help="The command will [b]show[/] information about what will be changed.",
    )
):
    """
    [green b]Sync[/] a [yellow]source[/] to [yellow]destination[/] for the [yellow b]backup plan id[/].
    [yellow b]Skips[/] if not available.
    """

    service: BackupSyncService = CurrentConfiguration.get(BackupSyncService)

    if id is None:
        console.print("[red b][ERROR][/] Backup plan does not exists!!!")
        return

    # TODO add args
    service.sync_by_plan_id(UUID(id), '-avuP', dry)


@sync.command()
def restore(
        id: str = typer.Option(
            None, "--id", "-i",
            help="[b]The id[/] is an uniq identification of a [b]backup plan[/].",
            show_default=False
        ),
        dry: bool = typer.Option(
            False, "-d", "--dry-run",
            help="The command will [b]show[/] information about what will be changed.",
        )
):
    """
    [green b]Sync[/] a [yellow]destination[/] to the [yellow]source[/] for the [yellow b]backup plan id[/].
    [yellow b]Skips[/] if not available.
    """

    service: BackupRestoreService = CurrentConfiguration.get(BackupRestoreService)
    choose_prompt: ChoosePromptFzf = CurrentConfiguration.get(ChoosePromptFzf)

    if id is None:
        console.print("[red b][ERROR][/] Backup plan does not exists!!!")
        return

    # TODO add args
    service.sync_by_plan_id(UUID(id), choose_prompt, '-avuP', dry)
