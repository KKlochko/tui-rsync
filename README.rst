tui-rsync
=========

|PyPI version|

tui-rsync is the application that will help you to manage yours backups.
It uses rsync for syncing backups.

Dependencies
============

-  rsync
-  fzf

Author
======

Kostiantyn Klochko (c) 2023-2025

License
=======

Under GNU GPL v3 license

.. |PyPI version| image:: https://badge.fury.io/py/tui-rsync.svg
   :target: https://badge.fury.io/py/tui-rsync
